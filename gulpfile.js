const gulp = require('gulp');
const gulpSass = require('gulp-sass');
const autoPrefixer = require('gulp-autoprefixer');
const gulpSourcemaps = require('gulp-sourcemaps');
const gulpImagemin = require('gulp-imagemin');

gulp.task('sass', () => {
    return gulp.src('./app/sass/main.scss')
        .pipe(gulpSourcemaps.init())
        .pipe(gulpSass({
            outputStyle: 'compressed'
        }))
        .pipe(autoPrefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulpSourcemaps.write())
        .pipe(gulp.dest('./dist/css'))
});

gulp.task('watch', () => {
    gulp.watch('./app/sass/**/*.scss', ['sass']);
});

gulp.task('imagemin', () => {
    gulp.src('./app/images/*.+(png|jpg)')
        .pipe(gulpImagemin())
        .pipe(gulp.dest('./dist/images'));
});